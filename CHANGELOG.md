# changelog getIp

tags utilisés: added  changed  cosmetic  deprecated  fixed  new rewriting  removed  syncro  security
date au format: YYYY-MM-DD

## [ Unreleased ]



## [ 2.15.1 ] - 2018.06.18
* syncro: f__random
* cosmetic: f_n_host, f_n_dig
* changed: syncro & renommage f__cnx -> f_n_cnx
* rewriting: f_ip_validate, syncro
* rewriting: f_n_dig, f_n_host
* rewriting: f_ip_pub, prise en charge f_n_host f_n_dig
* added: f_n_host
* added: compatibily for ArchLinux, drill instead dig, f_n_dig, f_ip_dns, f_ip_master, f_ip_pub

## [ 2.14.2 ] - 2018.06.16

* added: drill for compatibility with archlinux, f__dig
* added: option --dev pour chargement version dev (si existante)
* rewriting: traitement options d'appel
* fixed: added f__trim

## [ 2.13.0 ] - 2018.06.16

* syncro: composants, fscript_update avec ses parametres d'appel,renommage f__test_cnx -> f__cnx
* cosmetic: affichage & help, lint
* fixed: f__cnx server ipv6, f_ip_pub
* fixed: ip publiques
* changed: plus de détection ip avec telnet ou nc (plus de services connus), figet_ip_pub -> f_ip_pub

## [ 2.12.0 ] - 2018.03.09

* rewriting: shellcheck
* cosmetic: suppression f__which, revue traitement options
* rewriting: f_affichage
* syncro: figet_ip_pub, f__test_cnx
* syncro: f__color f__info f__log f__requis f__sudo f__user 
* syncro: fscript_cronAnacron fscript_get_version fscript_update

## [ 2.11.1 ] - 2018.03.04

* cosmetic:
* syncro:

## [ 2.11.0 ] - 2018.03.03

* syncro: fscript_install, fscript_remove, fscript_update
* syncro: f__color, f__dir, f__info, f__sudo, f__user, f__wcv, f__wget_test, figet_ip
* rewriting: prg_init
* fixed: figet_ip_pub, ubuntu 16.04

## [ 2.10.0 ] - 2018.02.11

* syncro: f__color
* rewriting: print_local, ipv6 dynamiques, ipv6 dépréciées
* rewriting: figet_ip, ipv6, traitemenent adresses dynamiques

## [ 2.9.0 ] - 2018.01.26

* rewriting: mineur, fscript_cronAnacron fscript_install fscript_remove fscript_update
* rewriting: f__requis
* fixed: f__sudo, extraction nb tentatives

## [ 2.7.0 ] - 2018.01.25

* rewriting: f__requis, commandes alternatives possible (gawk|mawk), mawk installé par défaut sur debian :(
* rewriting: test avec mawk, f__user, figet_ip
* rewriting: plus d'imposition gawk only, prg_init

## [ 2.6.1 ] - 2018.01.24

* added: cumul options (opérations) possibles pour la plupart des opérations
* rewriting: invocation f__sudo dans traitement options, plus confortable si su & _all_
* rewriting; f__wget_test
* rewriting: f_sudo abandonné dans fscript_install et fscript_remove, au profit appel au traitement général des options
* rewriting: f_help, f_affichage
* rewriting: général wget_log: fscript_get_version, fscript_update
* removed: liens frama.link 

## [ 2.5.0 ] - 2018.01.14

* rewriting: f_sudo, format nombre de tentatives et options appel possibles > 1

## [ 2.4.0 ] - 2018.01.12

* fixed: correction commentaire fscript_get_version

## [ 2.3.0 ] - 2017.12.29

* syncro: composants

## [ 2.2.0 ] - 2017.12.26

révision, synchro: f__info, option combinée raw:log

## [ 2.1.0 ] - 2017.12.24

* syncro: nouveau composants scripts
* rewriting: figet_ip_pub, wget, définition logs, pour cause de bug wget? sur testing
* fixed: f__wget_test, incompatible avec redirection logs

## [ 1.20.1 ] - 2017.12.15

* rewriting: affichage interface sortie ipv4 & ipv6

## [ 1.20.0 ] - 2017.12.14

* rewriting: figer_ip + f__dir
* syncro: getInfo

## [ 1.19.0 ] - 2017.12.11

* rewriting: figet_ip_pub
* rewriting: f__wget_test

## [ 1.18.0 ] - 2017.12.10

* rewriting: figet_ip

## [ 1.17.0 ] - 2017.12.6

* rewriting: fscript_update, controle chargement début et fin
* rewriting: changement séquence start pour éviter erreur cron

## [ 1.16.2 ] - 2017.12.5

* added: option - ( ou -46 ), ipv4 & ipv6
* added: prg_init
* rewriting: synchro fonctions communes
* rewriting: renommage $user_
* rewriting: démarrage
* rewriting: fscript_cronAnacron, fscript_install, fscript_remove, fscript_update, f__log, renommage  $fileInstall $fileLogs
* fixed: f__wget_test
* fixed: erreur cron

## [ 1.15.0 ] - 2017.10.28

* added: option -us (maj an place)

## [ 1.14.0 ] - 2017.10.27

* rewriting: f__log: plus d'avertissement si filelog absent, création
* rewriting: figet_ip_pub, renommage fonction & variable public
* rewriting: figet_ip, renommage variable public
* rewriting: f__cmd_exist: nouveau émule et remplace which debian
* rewriting: f__requis: pluriel, formatage code
* rewriting: f__sudo : fix fonctionnement avec sudo
* rewriting: f__user: fonctionnement en root only en console
* rewriting: f__wget_test, fscript_get_version, fscript_update, suppression option tries personnalisée
* cosmetic: fscript_cronAnacron
* rewriting: fscript_update: update spécial

## [ 1.13.1 ] - 2017.10.17

* rewriting: figet_ip_public suppression option tries personnalisée

## [ 1.13.0 ] - 2017.10.16

* fixed: figet_ip, erreur possible entre lo/certain ifn
* syncro: f__error f__info f__requis f__wget_test 

## [ 1.12.0 ] - 2017.10.11

* fixed: f__sudo : fonctionnement avec sudo

## [ 1.11.0 ] - 2017.10.08

* rewriting: f__wget_test(): nouvelle option test, nouveau nommage fichier temp
* rewriting: f__user, premier essai root only, fonctionnement en root only en console
* rewriting: figet_ip figet_ip_public
* added: test bash4 au démarrage
* rewriting: f__color: utilisation terminfo pour retour au std (et non noir), donc modifs:
    * f__color f__error f__info f__wget_test
    * fscript_get_version fscript_install fscript_remove fscript_update
* added: f__sudo dans install & remove script

## [ 1.10.0 ] - 2017.09.23

* syncro: f__requis, f__info, f__error	unset/for
* rewriting: unset/for

## [ 1.9.0 ] - 2017.09.07

* fixed: f_help
* rewriting: f__wget_test, fscript_get_version, f__log

## [ 1.8.0 ] - 2017.09.06

* syncro: fscript_cronAnacron, fscript_update, fscript_install, fscript_remove

## [ 1.7.0 ] - 2017.09.04

* added: IFS
* rewriting: figet_ip: 
   * ifnames, toutes les interfaces et pas seulement les connectées
   * mac address des interfaces
* added: affichage adresses mac

## [ 1.6.0 ] - 2017.09.03

* changed: test connectivité avant recherche ip public pour éviter timeout, figet_ip_public
* rewriting: figet_ip: 2 espaces préliminaires pour meilleure présentation

## [ 1.5.1 ] - 2017.09.01

* rewriting: f__wget_test

## [ 1.5.0 ] - 2017.08.30

* rewriting: f__requis, f__user, f__wget_test, fscript_cronAnacron
* rewriting: déclaration local

## [ 1.4.0 ] - 2017.08.30

* rewriting: conditions d'utilisations, fscript_install, fscript_remove(), fscript_update
* rewriting: appel, fscript_install, fscript_remove(), fscript_update
* cosmetic: f_help 

## [ 1.3.0 ] - 2017.08.28

* fixed: localisation fileDev
* rewriting: f__wget_test

## [ 1.2.0 ] - 2017.08.27

* fixed: upgrade
* fixed: install/remove
* rewriting: fscript_cronAnacron, fscript_install : changement lognameDev ->fileDev
* rewriting: présentation fscript_get_version, fscript_install, fscript_remove

## [ 1.0.3 ] - 2017.08.26

* cosmetic: fscript_dl en fscript_update
* rewriting: fscript_install, fscript_update, fscript_get_version
* rewriting: f__wget_test
* rewriting: f__error, f__info
* syncro: fscript_install pour éventuel fscript_install_special

## [ 0.9.0 ] - 2017.08.24

* cosmetic: fonctions core

## [ 0.8.2 ] - 2017.08.23

* rewriting: changement délais anacron, fscript_cronAnacron

## [ 0.8.1 ] - 2017.08.22

* syncro: fscript_dl

## [ 0.8.0 ] - 2017.08.21

* révison: f__user
* syncro: fscript_dl, fscript_install, fscript_remove, f__info

## [ 0.7.9 ] - 2017.08.20

* fixed: bug: fscript_cronAnacron appel fscript_cronAnacron_special
* fixed: $TERM

## [ 0.7.8 ] - 2017.08.19

* syncro: fscript_cronAnacron & plus de redémarrage service cron inutile & fonction spécifique pour certains scripts service cron restart &>/dev/null || /etc/init.d/cron restart &>/dev/null || f__info "redémarrer cron ou le PC"
* rewriting: fscript_dl plus de sortie progression download
* rewriting: fscript_get_version inclut version en cours
* rewriting: fscript_install mise en page
* syncro: début script 

## [ 0.7.5 ] - 2017.08.18

* syncro: fscript_cronAnacron lors upgrade et spécial pour dev 
* rewriting: ip locales
* added: protection absence iproute
* rewriting: test sur which
* added: f__requis, vérification requis pour fonctionnement script
* syncro: fscript_get_version, fscript_dl, fscript_install

## [ 0.7.1 ] - 2017/08/17

* added: test inscription crontab pour recherche bug siduction
* added: test crontab et modif anacrontab lors upgrade

## [ 0.6.0 ] - 2017/08/16

* rewriting: et homogénéisation des fonctions de script
* rewriting: f__user
* rewriting: f__requis
