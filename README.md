# getIp


![version: 2.15.1](https://img.shields.io/badge/version-2.15.1-blue.svg?longCache=true&style=for-the-badge)
![bash langage](https://img.shields.io/badge/bash-4-brightgreen.svg?longCache=true&style=for-the-badge)
![license LPRAB / WTFPL](https://img.shields.io/badge/license-LPRAB%20%2F%20WTFPL-blue.svg?longCache=true&style=for-the-badge)


> * script bash qui affiche les Ips publiques ou locales du PC
> * l'installation du script est possible en option. il se mettra alors éventuellement à jour automatiquement
> * l'option -us permet la mise à jour du script en place, sans installation et sans test ultérieur de 
    nouvelle version 
> * jusqu'à en trouver un, les utilitaires suivants seront utilisés: dig (dnsutils) / wget / curl / telnet / nc
> * script testé sur debian / ubuntu, mais devrait être compatible avec d'autres distributions


## chargement du script:

```shell
wget -nv -O getip https://framaclic.org/h/getip
chmod +x getIp
```

 
## help
`./getIp -h` ou `getIp -h` (si installé)
```text
              _   ___       
    __ _  ___| |_|_ _|_ __  
   / _' |/ _ \ __|| || '_ \ 
  | (_| |  __/ |_ | || |_) |
   \__, |\___|\__|___| .__/ 
   |___/  2.11.0     |_|  03/03/2018
  -----------------------------------------------------------------------
  ./getIp : exécution script
  getIp   : exécution script installé dans le système
  
  ce script requiert une des commandes suivantes pour déterminer l'ip publique:
     dig (dnsutils) | wget | curl | telnet | nc
  
  options:
    -4, --ip4     : affiche ipv4 public
    -6, --ip6     : affiche ipv6 public
    -46 --public  : affiche ipv4 & ipv6 public
    '', --local   : affiche adresses mac / ip privées / passerelle
    -us           : upgrade spécial du script en place (sans être installé)
  -----------------------------------------------------------------------
  ./getIp -i          : installation du script dans le système (root)
  getIp -h, --help    : affichage aide
  getIp -r, --remove  : désinstallation du script (root)
  getIp -u, --upgrade : mise à jour script
  getIp -v, --version : version du script

  plus d'infos: https://framaclic.org/h/doc-getip

```


## IPs locales

`./getIp` ou `getIp` (si installé)
```text
              _   ___       
    __ _  ___| |_|_ _|_ __  
   / _' |/ _ \ __|| || '_ \ 
  | (_| |  __/ |_ | || |_) |
   \__, |\___|\__|___| .__/ 
   |___/  2.11.0     |_|  03/03/2018

        IPv4  
  wlp3s0 (wifi)    : 192.168.1.146/24 
  
  lo (loopback)    : 127.0.0.1/8  

  passerelle: 192.168.1.5   (wlp3s0)  

        IPv6  
  wlp3s0 (wifi)    : 2a01:e35:8bf2:37b0:115c:6eaf:e33a:5f10/64 (lft 13224s) global temporary dynamic  
  wlp3s0 (wifi)    : 2a01:e35:8bf2:37b0:863a:4bff:fe30:3c28/64 (lft 86376s) global dynamic mngtmpaddr noprefixroute  
  
  wlp3s0 (wifi)    : fe80::863a:4bff:fe30:3c28/64  link noprefixroute
  lo (loopback)    : ::1/128  

  passerelle: fe80::160c:76ff:fe54:7490  (wlp3s0)  

  interfaces sortantes ipv6: wlp3s0 (2a01:e35:8bf2:37b0:115c:6eaf:e33a:5f10)  
  interfaces sortantes ipv4: wlp3s0 (192.168.1.146)  

        adresses Mac:  
  enp0s25: 3c:97:0e:28:19:4a
  wlp3s0: 84:3a:4b:85:03:28

        adresses ipv6 dépréciées:  
  wlp3s0 (wifi)    : 2a01:e35:8bf2:37b0:78e8:22fd:d7c0:24f9/64 (lft 0s) global temporary deprecated dynamic  
  wlp3s0 (wifi)    : 2a01:e35:8bf2:37b0:54f4:9ca7:e78a:feb3/64 (lft 0s) global temporary deprecated dynamic  
  wlp3s0 (wifi)    : 2a01:e35:8bf2:37b0:f459:faf9:a4ba:ebc3/64 (lft 0s) global temporary deprecated dynamic  
  wlp3s0 (wifi)    : 2a01:e35:8bf2:37b0:c7e:f94a:a553:73b3/64  (lft 0s) global temporary deprecated dynamic  
  wlp3s0 (wifi)    : 2a01:e35:8bf2:37b0:fcd7:315a:f118:d385/64 (lft 0s) global temporary deprecated dynamic  
  wlp3s0 (wifi)    : 2a01:e35:8bf2:37b0:b14a:7f8e:e61a:1a7c/64 (lft 0s) global temporary deprecated dynamic    

```


## IP publiques

`./getIp -46` ou `getIp -46` (si installé)  
```text
185.126.105.135
2a01:e0c:8bd5:13b0:e8ef:5cdb:f9a8:d308
```
ou
```text
pas de connectivité ipv4
pas de connectivité ipv6
```

## IP publiques ipv4

`./getIp -4` ou `getIp -4` (si installé)  
```text
185.126.105.135
```
ou
```text
pas de connectivité ipv4
```


## IP publiques ipv6

`./getIp -6` ou `getIp -6` (si installé)  
```text
2a01:e0c:8bd5:13b0:e8ef:5cdb:f9a8:d308
```
ou
```text
pas de connectivité ipv6
```


## mise à jour sans installation

* `./getInfo -us` (update spécial), le script est mis à jour là où il est, sans installation pré-requise


## installation du script

* `./getIp -i` **droits root** requis. Installation du script dans le système. Le script téléchargé sera 
  effacé du répertoire courant. Le lancement se fera donc par `getIp`.
* `getIp -r` **droits root** requis. Suppression du script du système. 
* `getIp -u` Mise à jour du script. Cette opération est automatiquement lancée toutes les semaines 
  si le script est installé.
* `getIp -v` Affiche la version du script et la version en ligne.

Une fois installé, le script est accessible à tous les utilisateurs. Un test hebdomadaire est effectué et le
script est mis à jour si une nouvelle version est disponible.

tous les évènements importants sont consignés dans le fichier _/var/log/sdeb_getIp.log_   
```shell
pager /var/log/sdeb_getIp.log
```


## sources

sur [framagit](https://framagit.org/sdeb/getIp/blob/master/getip)


## contact

pour tout problème ou suggestion concernant ce script, n'hésitez pas à ouvrir une issue 
[Framagit](https://framagit.org/sdeb/getIp/issues)

IRC: ##sdeb@freenode.net


## license

[LPRAB/WTFPL](https://framagit.org/sdeb/getIp/blob/master/LICENSE.md)


![compteur](https://framaclic.org/h/getip-gif)
